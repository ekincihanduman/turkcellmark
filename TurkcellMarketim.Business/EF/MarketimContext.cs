﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Business.EF.Abstract;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;

namespace TurkcellMarketim.Business.EF
{
    public class MarketimContext : IMarketimContext
    {
        public EfLogTypes _logTypes;
        //private readonly EfOpTurkcellMessageParameters _opTurkcellMessageParameters;
        //private readonly EfOpTurkcellMessages _opTurkcellMessages;
        //private readonly EfServices _services;
        //private readonly EfServiceProvisionLog _serviceProvisionLog;
        //private readonly EfServiceSentSmsLog _serviceSentSmsLog;
        public MarketimContext(EfLogTypes logTypes)
        {
            _logTypes = logTypes;
        }

        public EfLogTypes EfLogTypes(EfLogTypes efLogTypes)
        {
            return efLogTypes;
        }

        public EfOpTurkcellMessageParameters EfOpTurkcellMessageParameters(EfOpTurkcellMessageParameters efOpTurkcellMessageParameters)
        {
            return efOpTurkcellMessageParameters;
        }

        public EfOpTurkcellMessages EfOpTurkcellMessages(EfOpTurkcellMessages efOpTurkcellMessages)
        {
            return efOpTurkcellMessages;
        }

        public EfServiceProvisionLog EfServiceProvisionLog(EfServiceProvisionLog efServiceProvisionLog)
        {
            return efServiceProvisionLog;
        }

        public EfServices EfServices(EfServices efServices)
        {
            return efServices;
        }

        public EfServiceSentSmsLog EfServiceSentSmsLog(EfServiceSentSmsLog efServiceSentSmsLog)
        {
            return efServiceSentSmsLog;
        }
    }
}
