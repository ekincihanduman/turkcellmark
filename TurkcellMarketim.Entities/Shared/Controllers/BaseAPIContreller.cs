﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TurkcellMarketim.Entities.Shared.Controllers
{
    [Authorize]
    public class BaseAPIContreller<TModel, TResult>
    {
        TResult result;
        //[HttpGet]
        //public new virtual TResult Get(int id)
        //{
        //    return result;
        //}
        //[HttpGet]
        //public async new virtual Task<TResult> GetAsync(int id)
        //{
        //    return await Task.FromResult<TResult>(result);
        //}
        [HttpPost]
        public new virtual TResult Post([FromBody]TModel model)
        {
            return result;
        }
        [HttpPost]
        public async new virtual Task<TResult> PostAsync([FromBody]TModel model)
        {
            return await Task.FromResult<TResult>(result);
        }
    }
}
