﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TurkcellMarketim.Entities.Shared.Models
{
    public class LoginTokenModel : ModelBase
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public override DateTime DateTime => DateTime.Now;
        public override Guid RowGuid => Guid.NewGuid();
    }
}
