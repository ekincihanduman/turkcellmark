﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using TurkcellMarketim.Entities.Shared.Controllers;

namespace TurkcellMarketim.Entities.Shared.Models
{
    public static class HelperFunction
    {   
        public static string  GetName(this ResultCode resultCode)
        {
            return (((DisplayAttribute)resultCode.GetType().GetMember(resultCode.ToString()).First().GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault()).Name);
        }
    }
}
