﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public class BusinessInteraction : IEntity
    {
        public int Id { get; set; }

        public IEnumerable<Item> Items;
    }
}
