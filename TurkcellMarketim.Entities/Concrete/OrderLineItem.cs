﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public class OrderLineItem : IEntity
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public string OfferId { get; set; }
        public string Action { get; set; }
        public int? OrderLineId { get; set; }
        public OrderLine OrderLine { get; set; }
        public IEnumerable<Item> Items { get; set; }
        public bool Continue { get; set; } = false;
        public DateTime CreatedDate { get; set; }
    }
}
