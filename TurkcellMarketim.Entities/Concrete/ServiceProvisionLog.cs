﻿using System;
using System.Collections.Generic;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class ServiceProvisionLog : IEntity
    {
        public int ProvisionId { get; set; }
        public string Msisdn { get; set; }
        public string Action { get; set; }
        public string TransactionId { get; set; }
        public string ServiceVariantId { get; set; }
        public string OfferId { get; set; }
        public string ProductId { get; set; }
        public int? ServiceLogId { get; set; }
        public DateTime? ProvisionTime { get; set; }

        public ServiceLogs ServiceLog { get; set; }
    }
}