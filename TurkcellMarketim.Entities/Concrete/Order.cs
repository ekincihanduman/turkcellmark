﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public class Order :IEntity
    {
        public Order()
        {
            OrderLines = new HashSet<OrderLine>();
        }
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public DateTime? RecordDate { get; set; }
        public int? ServiceLogId { get; set; }
        public ServiceLogs ServiceLog { get; set; }
        public IEnumerable<OrderLine> OrderLines { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
