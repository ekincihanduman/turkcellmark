﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class OpTurkcellMessageParameters : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string ParameterContent { get; set; }
        public int? OpTurkcellMessageId { get; set; }

        public OpTurkcellMessages OpTurkcellMessage { get; set; }
    }
}