﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public class OrderLine : IEntity
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int? ServiceLogId { get; set; }
        public ServiceLogs ServiceLogs { get; set; }
        public string ProductId { get; set; }
        public string LineOfferId { get; set; }
        public string IdentifierForLineOfferId { get; set; }
        public IEnumerable<OrderLineItem> LineItemField;
        public DateTime CreatedDate { get; set; }
    }
}
