﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Services;

namespace TurkcellOutboundService.DataSource.Repositories.EntityModel
{
    //[DataContract(Namespace = "http://partnerprovisioningservice.turkcell.services.ws.fourplay.com.tr")]
    public class PartnerServiceProvisioningResponse
    {
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer", Order = 0)]
        public string statusCode { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string errorCode { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string errorDescription { get; set; }
    }
}