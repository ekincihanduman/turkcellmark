﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurkcellOutboundService.DataSource.Repositories.EntityModel
{
    public class PartnerServiceProvisioningRequest
    {
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string msisdn {get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 1)]
        public string payerMsisdn { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string action { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 3)]
        public string transactionId { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 4)]
        public string serviceVariantId { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 5)]
        public string offerId { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 6)]
        public string productId { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 7)]
        public string oldMsisdn { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 8)]
        public Nullable<decimal> finalPrice { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 9)]
        public campaignprofileitem campaignProfile { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 10)]

        public attribute[] subscriptionAttributes;
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 11)]

        public attribute[] provisioningAttributes;
    }
    public partial class attribute 
    {
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string key { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string value { get; set; }
    }
    public partial class campaignprofileitem 
    {
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 0)]
        public string campaignId { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 1)]
        public System.Nullable<System.DateTime> campaignStartDate { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 2)]
        public System.Nullable<System.DateTime> campaignEndDate { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 3)]
        public System.Nullable<System.DateTime> nextChargingDate { get; set; }
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 4)]
        public System.Nullable<System.DateTime> commitmentDate { get; set; }

    }
}