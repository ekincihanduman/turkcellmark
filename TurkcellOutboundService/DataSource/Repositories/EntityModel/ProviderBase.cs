﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Turkcellmarketim.ProvisionService;
using TurkcellOutboundService.DataSource.Repositories.EntityModel;
using TurkcellOutboundService.Sources;

namespace ProvisionService.Data.Repositories.EntityModel
{
    public class ProviderBase<TModel> : WebService
    {
        private static TModel _model;
        private static int _servicLogId;
        private static int _t_Id;
        private static ServiceLogger _serviceLog;
        public static ServiceLogger ServiceLogger
        {
            get { return _serviceLog; }
            set { _serviceLog = value; }
        }
        public static int T_ID
        {
            get { return _t_Id; }
            set { _t_Id = value; }
        }
        public static int ServiceLogId
        {
            get { return _servicLogId; }
            set { _servicLogId = value; }
        }
        public static TModel Model
        {
            get { return _model; }
            set { _model = value; }
        }

    }
}