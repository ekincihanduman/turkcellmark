﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using TurkcellOutboundService.DataSource.EF;
using TurkcellOutboundService.DataSource.Repositories.Interfaces;

namespace TurkcellOutboundService.DataSource.Repositories
{
    public class ApplicationRepositroy<TEntity> : IApplicationRepositroy<TEntity> where TEntity : class, new()
    {
        private TurkcellMarketimDBEntities _context;
        public ApplicationRepositroy()
        {
            _context = _context ?? new TurkcellMarketimDBEntities();
        }
        public TEntity Add(TEntity entity)
        {
            var addedEntity = _context.Entry(entity);
            addedEntity.State = EntityState.Added;
            _context.SaveChanges();
            return entity;
        }
        public async Task<TEntity> AddAsync(TEntity entity)
        {
            var addedEntity = _context.Entry(entity);
            addedEntity.State = EntityState.Added;
            await _context.SaveChangesAsync();
            return entity;
        }
        public void Delete(TEntity entity)
        {
            var addedEntity = _context.Entry(entity);
            addedEntity.State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null ? _context.Set<TEntity>().ToList() : _context.Set<TEntity>().Where(filter).ToList();
        }

        public TEntity GetT(Expression<Func<TEntity, bool>> filter = null)
        {
            return _context.Set<TEntity>().SingleOrDefault(filter);
        }

        public TEntity Update(TEntity entity)
        {
            try
            {
                _context.Entry(entity).State =  EntityState.Modified;
                _context.SaveChanges();
                return entity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}