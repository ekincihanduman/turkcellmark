//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TurkcellOutboundService.DataSource.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class OpTurkcellMessages
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OpTurkcellMessages()
        {
            this.OpTurkcellMessageParameters = new HashSet<OpTurkcellMessageParameters>();
        }
    
        public int Id { get; set; }
        public string Channel { get; set; }
        public string LargeAccount { get; set; }
        public string MessageType { get; set; }
        public string MpStringContent { get; set; }
        public string ReceiverMsIsdn { get; set; }
        public Nullable<System.DateTime> RecordDate { get; set; }
        public string SeasonId { get; set; }
        public Nullable<int> ServiceLogId { get; set; }
        public string VariantId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OpTurkcellMessageParameters> OpTurkcellMessageParameters { get; set; }
        public virtual ServiceLogs ServiceLogs { get; set; }
    }
}
