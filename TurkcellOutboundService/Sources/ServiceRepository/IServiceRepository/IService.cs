﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurkcellOutboundService.Sources.AuthRepository.IAuthRepository
{
    public interface IService
    {
        string ServiceUrl { get; set; }
        string CurrentToken { get; set; }
        RequestMethod RequestMethod { get; set; }
        string GetService(string parameters);
        Task<string> GetServiceAsync(Dictionary<string, string> parameters);
        string GetServiceAsync(string raw);
    }
}
