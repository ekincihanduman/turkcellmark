﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TurkcellOutboundService.Sources.AuthRepository.IAuthRepository;

namespace TurkcellOutboundService.Sources.AuthRepository
{
    public class APIService : IService
    {
        #region CreateToken
        public string ServiceUrl { get; set; }
        public string CurrentToken { get; set; } = "";
        public RequestMethod RequestMethod { get; set; } = RequestMethod.POST;
        public string GetService(string postData)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                //string postData = HelperFunction.GetServiceParameter(parameters);
                byte[] dataStream = Encoding.UTF8.GetBytes(postData);
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(ServiceUrl);
                webRequest.Method = (((DisplayAttribute)RequestMethod.POST.GetType().GetMember(RequestMethod.POST.ToString()).First().
                    GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault()).Name);

                if (CurrentToken.Length > 0)
                    webRequest.Headers.Add(HttpRequestHeader.Authorization, CurrentToken);
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = dataStream.Length;
                webRequest.KeepAlive = false;
                string responseFromServer = "";

                using (Stream newStream = webRequest.GetRequestStream())
                {
                    newStream.Write(dataStream, 0, dataStream.Length);
                    newStream.Close();
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseFromServer = reader.ReadToEnd();
                        reader.Close();
                    }

                    webResponse.Close();
                }

                return responseFromServer;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string CreateToken()
        {

            string parameters = "{ \"username\": \"Marketim\", \"password\": \"Angel4you!\" }";
            ServiceUrl = $"{ HelperFunction.GetAPIEndPoint()}/token";

            JObject resultData = JObject.Parse(GetService(parameters));
            if (resultData.HasValues && resultData["content"] != null)
            {
                CurrentToken = resultData["content"].ToString();
            }

            return CurrentToken;
        }
        public async Task<string> GetServiceAsync(Dictionary<string, string> parameters)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                string postData = HelperFunction.GetServiceParameter(parameters);
                byte[] dataStream = Encoding.UTF8.GetBytes(postData);
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(ServiceUrl);
                webRequest.Method = (((DisplayAttribute)RequestMethod.POST.GetType().GetMember(RequestMethod.POST.ToString()).First().
                    GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault()).Name);

                if (CurrentToken.Length > 0)
                    webRequest.Headers.Add(HttpRequestHeader.Authorization, CurrentToken);
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = dataStream.Length;
                webRequest.KeepAlive = false;
                string responseFromServer = "";

                using (Stream newStream = await webRequest.GetRequestStreamAsync())
                { 
                    newStream.Write(dataStream, 0, dataStream.Length);
                    newStream.Close();
                }

                using (WebResponse webResponse = await webRequest.GetResponseAsync())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseFromServer = await reader.ReadToEndAsync();
                        reader.Close();
                    }

                    webResponse.Close();
                }

                JObject resultData = JObject.Parse(responseFromServer);
                if (resultData.HasValues && resultData["access_token"] != null)
                {
                    return resultData["access_token"].ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string GetServiceAsync(string raw)
        {
            try
            {
                CreateToken();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServiceUrl = $"{ HelperFunction.GetAPIEndPoint()}serviceOrderManagement";

                byte[] dataStream = Encoding.UTF8.GetBytes(raw);
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(ServiceUrl);
                webRequest.Method = (((DisplayAttribute)RequestMethod.POST.GetType().GetMember(RequestMethod.POST.ToString()).First().
                    GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault()).Name);

                if (CurrentToken.Length > 0)
                    webRequest.Headers.Add(HttpRequestHeader.Authorization, CurrentToken);
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = dataStream.Length;
                webRequest.KeepAlive = false;
                string responseFromServer = "";

                using (Stream newStream = webRequest.GetRequestStream())
                {
                    newStream.Write(dataStream, 0, dataStream.Length);
                    newStream.Close();
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(webResponse.GetResponseStream()))
                    {
                        responseFromServer = reader.ReadToEnd();
                        reader.Close();
                    }

                    webResponse.Close();
                }

                return responseFromServer;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion
    }
}
