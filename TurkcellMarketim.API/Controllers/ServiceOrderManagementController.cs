﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Turkcell.Service.OrderManagementV2;
using TurkcellMarketim.API.Models;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.Entities.Concrete;
using TurkcellMarketim.Entities.Shared.Controllers;
using TurkcellMarketim.Entities.Shared.Models;

namespace TurkcellMarketim.API.Controllers
{
    [Produces("application/json")]
    [Route("api/ServiceOrderManagement")]
    public class ServiceOrderManagementController : Controller
    {
        private readonly ILogTypes _efLogTypes;
        private readonly IServiceLogs _serviceLogs;
        private readonly IOrder _order;
        private readonly IOrderLines _orderLines;
        private readonly IOrderLineItems _orderLineItems;
        private readonly IBusinessInteraction _businessInteraction;
        private readonly IItems _items;
        public ServiceOrderManagementController(ILogTypes efLogTypes, IServiceLogs serviceLogs, IOrder order, IOrderLines orderLines, IOrderLineItems orderLineItems, IBusinessInteraction businessInteraction, IItems items)
        {
            _efLogTypes = efLogTypes;
            _serviceLogs = serviceLogs;
            _order = order;
            _orderLines = orderLines;
            _orderLineItems = orderLineItems;
            _businessInteraction = businessInteraction;
            _items = items;
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<APIResult> CreateOrder([FromBody]CreateOrderRequest request )
        {
            TurkcellOperator op = new TurkcellOperator();
            ServiceOrderManagementResponse response =  await op.OrderManagemenV2(request);

            var orderResuslt = _order.GetT(x => x.OrderCode == response.orderId);
            
            Order order = new Order
            {
                OrderCode = response.orderId,
                RecordDate = DateTime.Now,
                CreatedDate = DateTime.Now
            };
            if (orderResuslt == null || orderResuslt.Id == 0)
            {
                order = _order.Add(order);
            }
            else
            {
                order = orderResuslt;
            }

            var serviceEntity = _serviceLogs.Add(
                new ServiceLogs
                {
                    Description = $"ServiceManagementV2 Order: { order.Id} ",
                    LogTypeId = (int)LogTypes.Ok,
                    ServiceId = (int)ServiceType.ServiceOrderManagementV2,
                    Msisdn = response.line.First().identifierForLineOfferId,
                    IsSuccessful = true,
                    RequestTime = DateTime.Now
                });
            order.ServiceLogId = serviceEntity.ServiceLogId;
            _order.Update(order);

            foreach (var orderLine in response.line)
            {
                var orderLineEntity = _orderLines.Add(new OrderLine
                {
                    IdentifierForLineOfferId = orderLine.identifierForLineOfferId,
                    OrderId = order.Id,
                    LineOfferId = orderLine.lineOfferId,
                    Order = order,
                    CreatedDate = DateTime.Now
                });
                serviceEntity = _serviceLogs.Add(
                  new ServiceLogs
                  {
                      Description = $"ServiceManagementV2 OrderLine: { orderLineEntity.Id}",
                      LogTypeId = (int)LogTypes.Ok,
                      Msisdn = orderLine.identifierForLineOfferId,
                      ServiceId = (int)ServiceType.ServiceOrderManagementV2,
                      IsSuccessful = true,
                      RequestTime = DateTime.Now
                    });
                orderLineEntity.ServiceLogId = serviceEntity.ServiceLogId;
                orderLineEntity = _orderLines.Update(orderLineEntity);

                foreach (var orderLineItem in orderLine.lineItem)
                {
                    var orderLineItemEntity = _orderLineItems.Add(
                    new OrderLineItem
                    {
                        OfferId = orderLineItem.offerId,
                        Action = orderLineItem.action,
                        Continue = orderLineItem.@continue,
                        OrderLineId = orderLineEntity.Id,
                        OrderLine = orderLineEntity,
                        ProductId = orderLineItem.productId,
                        CreatedDate = DateTime.Now
                    });

                    if (orderLineItem.businessInteraction != null)
                    {
                        foreach (smbusinessinteractionerror item in orderLineItem.businessInteraction.Items)
                        {
                            _items.Add(
                                new Item
                                {
                                    ErrorId = Convert.ToInt32(item.errorId),
                                    NotificationId = item.notificationId,
                                    ErrorDetail = item.errorDetail,
                                    Type = item.type,
                                    UserText = item.userText,
                                    OrderLineItemId = orderLineItemEntity.Id,
                                    OrderLineItem = orderLineItemEntity,
                                    CreatedDate = DateTime.Now
                                });
                        }
                    }
                }

                order.OrderLines = _orderLines.GetList(x => x.OrderId == order.Id);
            }

            return new APIResult
            {
                Content = order,
                ResultCode = (int)ResultCode.Success,
                ResultMessage = "İşlem Başarılı"
            };
        }
    }
}