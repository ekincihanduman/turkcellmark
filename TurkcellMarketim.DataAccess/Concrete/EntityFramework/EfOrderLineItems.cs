﻿using TurkcellMarketim.Core.DataAccess.EntityFramework;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Concrete.EntityFramework
{
    public class EfOrderLineItems : EfEntityRepositoryBase<OrderLineItem, MarketimDBContext>, IOrderLineItems
    {

    }
}
