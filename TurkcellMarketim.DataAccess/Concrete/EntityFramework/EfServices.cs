﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.DataAccess.EntityFramework;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Concrete.EntityFramework
{
    public class EfServices : EfEntityRepositoryBase<Services, MarketimDBContext>, IServices
    {
    }
}
