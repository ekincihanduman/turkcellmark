﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Concrete.EntityFramework
{
    public partial class MarketimDBContext : IdentityDbContext<ApplicationUser>
    {
        private const string _dbConnection = @"Server=turkcellmarketimdb.cghxvnbvaxd5.eu-west-3.rds.amazonaws.com,1433;Database=MarketimDB_new;User Id=kyros;Password=Dvl*2017;MultipleActiveResultSets=true;";
       
        public virtual DbSet<LogTypes> LogTypes { get; set; }
        public virtual DbSet<OpTurkcellMessageParameters> OpTurkcellMessageParameters { get; set; }
        public virtual DbSet<OpTurkcellMessages> OpTurkcellMessages { get; set; }
        public virtual DbSet<ServiceLogs> ServiceLogs { get; set; }
        public virtual DbSet<ServiceProvisionLog> ServiceProvisionLog { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<ServiceSentSmsLog> ServiceSentSmsLog { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderLine> OrderLines { get; set; }
        public virtual DbSet<OrderLineItem> OrderLineItems { get; set; }
        //public virtual DbSet<BusinessInteraction> BusinessInteractions { get; set; }
        public virtual DbSet<Item> Items { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_dbConnection);
            }
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<LogTypes>(entity =>
            {
                entity.HasKey(e => e.LogTypeId);

                entity.Property(e => e.LogDescription).HasMaxLength(100);
            });

            modelBuilder.Entity<OpTurkcellMessageParameters>(entity =>
            {
                entity.Property(e => e.ParameterContent).HasMaxLength(160);

                entity.HasOne(d => d.OpTurkcellMessage)
                    .WithMany(p => p.OpTurkcellMessageParameters)
                    .HasForeignKey(d => d.OpTurkcellMessageId)
                    .HasConstraintName("FK_OpTurkcellMessageParameters_OpTurkcellMessages");
            });

            modelBuilder.Entity<OpTurkcellMessages>(entity =>
            {
                entity.Property(e => e.Channel).HasMaxLength(10);

                entity.Property(e => e.LargeAccount).HasMaxLength(100);

                entity.Property(e => e.MessageType).HasMaxLength(30);

                entity.Property(e => e.MpStringContent).HasMaxLength(160);

                entity.Property(e => e.ReceiverMsIsdn).HasMaxLength(16);

                entity.Property(e => e.RecordDate).HasColumnType("datetime");

                entity.Property(e => e.VariantId).HasMaxLength(4000);

                entity.HasOne(d => d.ServiceLog)
                    .WithMany(p => p.OpTurkcellMessages)
                    .HasForeignKey(d => d.ServiceLogId)
                    .HasConstraintName("FK_OpTurkcellMessages_ServiceLogs");
            });

            modelBuilder.Entity<ServiceLogs>(entity =>
            {
                entity.HasKey(e => e.ServiceLogId);

                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.Msisdn).HasMaxLength(50);

                entity.Property(e => e.RequestTime).HasColumnType("datetime");

                entity.HasOne(d => d.LogType)
                    .WithMany(p => p.ServiceLogs)
                    .HasForeignKey(d => d.LogTypeId)
                    .HasConstraintName("FK_ServiceLogs_LogTypes");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.ServiceLogs)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("FK_ServiceLogs_Services");
            });

            modelBuilder.Entity<ServiceProvisionLog>(entity =>
            {
                entity.HasKey(e => e.ProvisionId);

                entity.Property(e => e.Action).HasMaxLength(30);

                entity.Property(e => e.Msisdn).HasMaxLength(50);

                entity.Property(e => e.OfferId).HasMaxLength(10);

                entity.Property(e => e.ProductId).HasMaxLength(10);

                entity.Property(e => e.ProvisionTime).HasColumnType("datetime");

                entity.Property(e => e.ServiceVariantId).HasMaxLength(10);

                entity.Property(e => e.TransactionId).HasMaxLength(30);

                entity.HasOne(d => d.ServiceLog)
                    .WithMany(p => p.ServiceProvisionLog)
                    .HasForeignKey(d => d.ServiceLogId)
                    .HasConstraintName("FK_ServiceProvisionLog_ServiceLogs");
            });

            modelBuilder.Entity<Services>(entity =>
            {
                entity.HasKey(e => e.ServiceId);

                entity.Property(e => e.ServiceName).HasMaxLength(500);
            });

            modelBuilder.Entity<ServiceSentSmsLog>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.Property(e => e.Msisdn).HasMaxLength(50);

                entity.Property(e => e.SendSms).HasMaxLength(4000);

                entity.Property(e => e.SentTime).HasColumnType("datetime");

                entity.HasOne(d => d.ServiceLog)
                    .WithMany(p => p.ServiceSentSmsLog)
                    .HasForeignKey(d => d.ServiceLogId)
                    .HasConstraintName("FK_ServiceSentSmsLog_ServiceLogs");
            });
        }
    }
}