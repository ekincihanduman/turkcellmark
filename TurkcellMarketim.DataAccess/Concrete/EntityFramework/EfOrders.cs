﻿using TurkcellMarketim.Core.DataAccess.EntityFramework;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Concrete.EntityFramework
{
    public class EfOrders : EfEntityRepositoryBase<Order, MarketimDBContext>, IOrder
    {

    }
}
