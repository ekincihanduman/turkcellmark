﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TurkcellMarketim.DataAccess.Migrations
{
    public partial class SessionInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SeasonId",
                table: "OpTurkcellMessages",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SeasonId",
                table: "OpTurkcellMessages",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
