﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TurkcellMarketim.DataAccess.Migrations
{
    public partial class ServiceOrderManagementv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "ItemValue",
                table: "Items",
                newName: "UserText");

            migrationBuilder.RenameColumn(
                name: "ItemKey",
                table: "Items",
                newName: "Type");

            migrationBuilder.AlterColumn<int>(
                name: "OrderLineItemId",
                table: "Items",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "ErrorDetail",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ErrorId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotificationId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items",
                column: "OrderLineItemId",
                principalTable: "OrderLineItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ErrorDetail",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ErrorId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "NotificationId",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "UserText",
                table: "Items",
                newName: "ItemValue");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "Items",
                newName: "ItemKey");

            migrationBuilder.AlterColumn<int>(
                name: "OrderLineItemId",
                table: "Items",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items",
                column: "OrderLineItemId",
                principalTable: "OrderLineItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
