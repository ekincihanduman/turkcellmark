﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.DataAccess;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Abstract
{
    public interface IServices : IEntityRepository<Services>
    {
    }
}
