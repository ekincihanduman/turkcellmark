﻿using System;
using Turkcell.Service.SendMessage;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;
using TurkcellMarketim.Entities.Concrete;
using TurkcellMarketim.Web.Models;

namespace TurkcellMarketim.Helpers
{
    class TurkcellOperator : IMobileOperator
    {
        private static EfServiceLogs _serviceLogs;

        public TurkcellOperator()
        {

        }
        public TurkcellOperator(EfServiceLogs serviceLogs)
        {
            _serviceLogs = serviceLogs;
        }

        public void SendSms(string msisdn, string smsBody)
        {
            ServiceResponse response;
            ServiceLogs serviceLog;
            SendSMSOutput o = TurkcellServiceHelper.SendFreeSms(new string[] { msisdn }, new string[] { smsBody });

            serviceLog = new ServiceLogs()
            {
                RequestTime = DateTime.UtcNow,
                ServiceId = (int)ServiceType.SendSMS,
                LogTypeId = (int)LogTypes.Ok,
                Description = null,
                Msisdn = msisdn,
                IsSuccessful = true
            };

            if (Helper.ControlSmsError(o))
            {
                response = Helper.SendSMSOutputCreator("0", "Sms Sent Successfully", "0");
                serviceLog.IsSuccessful = true;
                serviceLog.Description = response.errorDescription;
            }
            else
            {
                serviceLog.IsSuccessful = false;
                serviceLog.LogTypeId = (int)LogTypes.Error;
                serviceLog.Description = o.TSOresult.errorDescription;
                response = new ServiceResponse() { errorCode = o.TSOresult.errorCode, errorDescription = o.TSOresult.errorDescription, statusCode = o.TSOresult.statusCode };
            }

            var serviceResult = ServiceLogger.SLog.Log(serviceLog);
            var smsResult = SmsLogger.SmsLoggs.Log(msisdn, smsBody, serviceResult);
        }
    }
}