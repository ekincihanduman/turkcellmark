﻿using System;

namespace TurkcellMarketim.Helpers
{
    public static class TurkcellServiceHelper
    {
        public static void CreateSession()
        {
            Turkcell.Service.Auth.Authentication authenticationService = new Turkcell.Service.Auth.AuthenticationClient();
            TurkcellAccesxToken.token = authenticationService.createSessionAsync("400023959", "611555924", "400023959").Result;
            TurkcellAccesxToken.generationTime = DateTime.UtcNow;
        }

        public static Turkcell.Service.SendMessage.SendSMSOutput SendFreeSms(string[] toReceivers, string[] message)
        {
            try
            {
                TurkcellServiceHelper.CreateSession();

                Turkcell.Service.SendMessage.SendSMSRequest sendSMSRequest = new Turkcell.Service.SendMessage.SendSMSRequest();

                Turkcell.Service.SendMessage.SendSMSInput sendSMSInput = new Turkcell.Service.SendMessage.SendSMSInput
                {
                    SHORT_NUMBER = "4601",
                    TO_RECEIVERS = toReceivers,
                    MESSAGE_BODY = message
                };

                Turkcell.Service.SendMessage.token token = new Turkcell.Service.SendMessage.token
                {
                    sessionId = TurkcellAccesxToken.token
                };

                sendSMSRequest.token = token;

                Turkcell.Service.SendMessage.SendMessagePortClient sendMessagePortClient = new Turkcell.Service.SendMessage.SendMessagePortClient();

                Turkcell.Service.SendMessage.SendSMSResponse sendSMSResponse = new Turkcell.Service.SendMessage.SendSMSResponse();

                sendSMSResponse = sendMessagePortClient.SendSMSAsync(token, sendSMSRequest.transactionlist, sendSMSInput).Result;

                return sendSMSResponse.SendSMSOutput;
            }
            catch (Exception ex)
            {
                TurkcellServiceHelper.CreateSession();
                Console.Out.WriteLine(ex.Message);
                return null;
            }
        }

        public static class TurkcellAccesxToken
        {
            public static string token { get; set; }
            public static DateTime generationTime { get; set; }
        }
    }
}