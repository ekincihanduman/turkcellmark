﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TurkcellMarketim.Helpers
{
    public class MobileOperatorFactory
    {
        public static IMobileOperator GetInstance(string mobileOperator)
        {
            if (mobileOperator == "Turkcell")
                return new TurkcellOperator();
            else
                return null;
        }
    }
}