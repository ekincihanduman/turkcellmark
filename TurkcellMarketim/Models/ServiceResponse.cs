﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurkcellMarketim.Web.Models
{
    public class ServiceResponse
    {
        public string statusCode { get; set; }
        public string errorCode { get; set; }
        public string errorDescription { get; set; }
    }
}